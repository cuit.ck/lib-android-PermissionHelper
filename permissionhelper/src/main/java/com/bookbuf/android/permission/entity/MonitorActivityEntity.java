package com.bookbuf.android.permission.entity;

import android.app.Activity;

import com.bookbuf.android.permission.inf.OnRequestPermissionsResultCallback;

/**
 * Created by robert on 16/5/27.
 */
public class MonitorActivityEntity extends MonitorEntity<Activity> {
	public MonitorActivityEntity (Activity activity, OnRequestPermissionsResultCallback callback) {
		super (activity, callback);
	}
}
