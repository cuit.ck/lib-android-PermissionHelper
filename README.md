# Android M 权限申请

[http://developer.android.com/training/permissions/index.html]
[http://developer.android.com/guide/topics/security/permissions.html]

有任何建议或反馈 [请联系: chenjunqi.china@gmail.com](mailto:chenjunqi.china@gmail.com) 

欢迎大家加入`android 开源项目群(369194705)`, 有合适的项目大家一起 `fork`;

你可能希望了解这些`adb shell`命令:
```
root@generic_x86:/ # pm list permissions 
root@generic_x86:/ $ pm revoke com.bookbuf.android android.permission.READ_CONTACTS
root@generic_x86:/ $ pm grant com.bookbuf.android android.permission.READ_CONTACTS

```

> 准备待请求/检查的权限

```java

	/*示例权限*/
	public PermEntityCompat[] exampleSinglePerm () {
		PermEntityCompat perm = new PermEntityCompat (Manifest.permission.READ_CONTACTS, "联系人权限", "读取联系人权限是为了xxxxxx");
		return new PermEntityCompat[]{perm};
	}

	public PermEntityCompat[] exampleGroupPerms () {
		PermEntityCompat[] perms = new PermEntityCompat[]{
				new PermEntityCompat (Manifest.permission.READ_CONTACTS, "联系人权限", "读取联系人权限是为了xxxxxx"),
				new PermEntityCompat (Manifest.permission.READ_CALENDAR, "日历权限", "读取日历权限是为了 xxxxxx"),
				new PermEntityCompat (Manifest.permission.READ_SMS, "短信权限", " 读取短信权限是为了 xxxxxx")
		};
		return perms;
	}

```

> 注册回调事件

```java
	
	public OnRequestPermissionsResultCallback callback () {
		if (callback != null) return callback;
		callback = new OnRequestPermissionsResultCallback<PermEntityCompat> () {
			@Override
			public void onGranted (PermEntityCompat... permission) {
				String[] perms = PermUtil.permissions (permission);
				Log.e (TAG, "onGranted : " + Arrays.toString (perms));
			}

			@Override
			public void onDenied (PermEntityCompat... permission) {
				String[] perms = PermUtil.permissions (permission);
				Log.e (TAG, "onDenied : " + Arrays.toString (perms));
			}

			@Override
			public void onChecked (boolean isGranted, PermEntityCompat... permission) {
				String[] perms = PermUtil.permissions (permission);
				Log.e (TAG, "onChecked : " + Arrays.toString (perms) + ":权限检查:" + isGranted);
			}
		};
		return callback;
	}
	

```

> 重写`Activity`的`onRequestPermissionsResult`方法:

```java

	@Override
	public void onRequestPermissionsResult (int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult (requestCode, permissions, grantResults);
		PermDelegateCompat.onRequestPermissionsResult (requestCode, permissions, grantResults);
	}

```

> 发起权限申请

```java

	PermDelegateCompat.Debug.register (this, callback ());
	// ...
	PermDelegateCompat.Client.requestIfNotAcquirePermission (exampleGroupPerms ());
	// ...
	PermDelegateCompat.Debug.unregister (this, callback ());

```

<img src="device-2016-05-30-144649.png" width = "300" height = "480" alt="" align=center />

> 非常感谢 项目[androidPermissionHelper](https://git.oschina.net/1and1get2/androidPermissionHelper)给予的一些参考;
