package com.bookbuf.android.permission.entity;

/**
 * 权限实例
 */
class PermEntity {

	/**
	 * {@link android.Manifest.permission}
	 */
	protected String permission;


	public PermEntity (String permission) {
		this.permission = permission;
	}

	public String getPermission () {
		return permission;
	}

	public void setPermission (String permission) {
		this.permission = permission;
	}


}
